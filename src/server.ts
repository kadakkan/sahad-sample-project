import express, { NextFunction, Request, Response } from "express";
import session from "express-session";
import dotenv from "dotenv";
import cors from "cors";
const mongoose = require("mongoose");
import { config } from "./config";
import passport from "passport";
import "./config/passport.config";
import http from "http";
import { Server } from "socket.io";
import { setupChat } from "./utility/socket.util";

dotenv.config();

const app = express();

app.post(
  "/api/user/webhook",
  express.raw({ type: "application/json" }),
  async (req: Request, res: Response) => stripeController.webhook(req, res),
);

app.post(
  "/api/payment/webhook",
  express.raw({ type: "application/json" }),
  async (req: Request, res: Response) =>
    stripeController.paymentWebhook(req, res),
);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());

app.use(
  session({
    secret: "your-secret-key",
    resave: false,
    saveUninitialized: false,
  }),
);

app.use(passport.initialize());
app.use(passport.session());

mongoose
  .connect(config.mongoose.databaseUrl)
  .then(() => console.log("Connected!"));

const server = http.createServer(app);
const io = new Server(server);
setupChat(io);

import userRoute from "./routes/user.route";
import blogRoute from "./routes/blog.route";
import testimonialRoute from "./routes/testimonial.route";
import packageRoute from "./routes/package.route";
import leadRoute from "./routes/lead.route";
import galleryRoute from "./routes/gallery.route";
import shopRoute from "./routes/shop.route";
import referRoute from "./routes/refer.route";
import designerRoute from "./routes/designer.route";
import uploadRoute from "./routes/upload.route";
import authRoute from "./routes/auth.route";
import addonRoute from "./routes/addon.route";
import orderRoute from "./routes/order.route";
import questionnaireRoute from "./routes/questionnaire.route";
import reviewRoute from "./routes/review.route";
import stripeController from "./controllers/stripe.controller";

app.use("/api/user", userRoute);
app.use("/api/blogs", blogRoute);
app.use("/api/testimonials", testimonialRoute);
app.use("/api/packages", packageRoute);
app.use("/api/leads", leadRoute);
app.use("/api/gallery", galleryRoute);
app.use("/api/shop", shopRoute);
app.use("/api/refer", referRoute);
app.use("/api/designer", designerRoute);
app.use("/api/upload", uploadRoute);
app.use("/api/auth", authRoute);
app.use("/api/addons", addonRoute);
app.use("/api/orders", orderRoute);
app.use("/api/questionnaire", questionnaireRoute);
app.use("/api/review", reviewRoute);

app.listen(config.port, () => {
  console.log("Server is running");
});
