import mongoose, { Document, Schema } from 'mongoose';

interface IBlog extends Document {
  title: string;
  description: string;
  author: string;
  blog_thumbnail: string;
  createdAt: Date;
}

const blogSchema = new Schema<IBlog>({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  blog_thumbnail: {
    type: String,
    required: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const BlogModel = mongoose.model<IBlog>('Blog', blogSchema);

export { BlogModel, IBlog };
