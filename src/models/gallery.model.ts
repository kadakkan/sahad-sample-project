import mongoose, { Document, Schema } from 'mongoose';

interface IGallery extends Document {
  project: Schema.Types.ObjectId;
  image_url: string;
  createdAt: Date;
}

const gallerySchema = new Schema<IGallery>({
  project: {
    type: Schema.Types.ObjectId,
    ref: 'Project',
    required: true,
  },
  image_url: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const GalleryModel = mongoose.model<IGallery>('Gallery', gallerySchema);

export { GalleryModel, IGallery };
