import mongoose, { Document, Schema } from 'mongoose';

interface ILead extends Document {
  full_name: string;
  email: string;
  phone_number: string;
  country: string;
  request_info: string;
  location: { latitude: number; longitude: number; address: string; };
  createdAt: Date;
}

const leadSchema = new Schema<ILead>({
  full_name: {
    type: String,
    required: false,
  },
  email: {
    type: String,
    required: true,
  },
  phone_number: {
    type: String,
    required: false,
  },
  country: {
    type: String,
    required: false,
  },
  request_info: {
    type: String,
    required: false,
  },
  location: {
    latitude: {
      type: Number,
      required: false,
    },
    longitude: {
      type: Number,
      required: false,
    },
    address: {
      type: String,
      required: false,
    },
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const LeadModel = mongoose.model<ILead>('Lead', leadSchema);

export { LeadModel, ILead };
