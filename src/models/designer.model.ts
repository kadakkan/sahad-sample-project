import mongoose, { Document, Schema } from 'mongoose';

interface IDesigner extends Document {
  is_approved: boolean;
  name: string;
  dob: Date;
  email: string;
  skills: { skill_name: string; level: number }[];
  identity_documents: { document_name: string; url: string }[];
  portfolio: { project_name: string; url: string }[];
  createdAt: Date;
}

const designerSchema = new Schema<IDesigner>({
  is_approved: {
    type: Boolean,
    required: true,
    default: false
  },
  name: {
    type: String,
    required: true,
  },
  dob: {
    type: Date,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  skills: [
    {
      skill_name: {
        type: String,
        required: true,
      },
      level: {
        type: Number,
        required: true,
      },
    },
  ],
  identity_documents: [
    {
      document_name: {
        type: String,
        required: true,
      },
      url: {
        type: String,
        required: true,
      },
    },
  ],
  portfolio: [
    {
      project_name: {
        type: String,
        required: true,
      },
      url: {
        type: String,
        required: true,
      },
    },
  ],
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const DesignerModel = mongoose.model<IDesigner>('Designer', designerSchema);

export { DesignerModel, IDesigner };

