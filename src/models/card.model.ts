import mongoose, { Document, Schema } from "mongoose";

interface ICard extends Document {
  stripe_customer_id: string;
  payment_method_id: string;
  user_id: Schema.Types.ObjectId;
  country: string;
  brand: string;
  exp_month: number;
  exp_year: number;
  is_default: boolean;
  last4: number;
  createdAt: Date;
}

const cardSchema = new Schema<ICard>({
  stripe_customer_id: { type: String, required: true },
  payment_method_id: { type: String, required: true },
  user_id: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: "User",
  },
  brand: { type: String, required: true },
  exp_month: { type: Number, required: true },
  exp_year: { type: Number, required: true },
  last4: { type: Number, required: true },
  is_default: { type: Boolean, required: true, default: false },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const CardModel = mongoose.model<ICard>("Card", cardSchema);

export { CardModel, ICard };
