import mongoose, { Schema, Document } from "mongoose";

interface IChat extends Document {
  user_id: Schema.Types.ObjectId;
  chat_name: string;
  last_send_user: {
    name: string;
    picture: string;
    message: string;
    _id: Schema.Types.ObjectId;
  };
  createdAt: Date;
}

interface IChatMessage extends Document {
  from_id: Schema.Types.ObjectId;
  from_name: string;
  from_pic: string;
  chat_id: Schema.Types.ObjectId;
  message: string;
  createdAt: Date;
}

const chatSchema = new Schema<IChat>({
  user_id: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  chat_name: {
    type: String,
    required: true,
  },
  last_send_user: {
    name: {
      type: String,
      required: false,
    },
    picture: {
      type: String,
      required: false,
    },
    message: {
      type:String,
      required: false
    },
    _id: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: false,
    },
  },
  createdAt: {
    type: Date,
    default: Date.now,
  }
});

const chatMessageSchema = new Schema<IChatMessage>({
  from_id: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  from_name: {
    type: String,
    required: false
  },
  from_pic: {
    type: String,
    required: false
  },
  chat_id: {
    type: Schema.Types.ObjectId,
    ref: "Chat",
    required: true,
  },
  message: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
});

const ChatModel = mongoose.model<IChat>("Chat", chatSchema);
const ChatMessageModel = mongoose.model<IChatMessage>("ChatMessage", chatMessageSchema);

export { ChatModel, IChat, ChatMessageModel, IChatMessage };
