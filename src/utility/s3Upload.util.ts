import multer from "multer";
import {
  S3Client,
  PutObjectCommand,
  CreateMultipartUploadCommand,
  UploadPartCommand,
  CompleteMultipartUploadCommand,
  AbortMultipartUploadCommand,
} from "@aws-sdk/client-s3";
import { config } from "../config";

const s3 = new S3Client({
  region: config.aws.awsRegion,
  credentials: {
    accessKeyId: config.aws.awsAccessKeyId,
    secretAccessKey: config.aws.awsSecretAccessKey,
  },
});
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";

const storage = multer.memoryStorage();
const upload = multer({ storage });

export const s3Upload = (
  files: Express.Multer.File[],
  folder: string
): Promise<{ filename: string; url: string }[]> => {
  return Promise.all(
    files.map((file) => {
      const command = new PutObjectCommand({
        Bucket: config.aws.awsBucketName!,
        Key: `${folder}/${file.originalname}`,
        Body: file.buffer,
        ACL: "public-read",
      });
      return s3.send(command).then(() => {
        return {
          filename: file.originalname,
          url: `https://${config.aws.awsBucketName}.s3.amazonaws.com/${folder}/${file.originalname}`,
        };
      });
    })
  )
    .then((data) => {
      return data;
    })
    .catch((err) => {
      return err;
    });
};

export const signedUrlUploadFile = async (fileName: string, folder: string, fileType: string) => {
  const command = new PutObjectCommand({
    Bucket: config.aws.awsBucketName,
    Key: `${folder}/${fileName}`,
    ACL: "public-read",
    ContentType: fileType,
  });
  try{
      const url = await getSignedUrl(s3, command, { expiresIn: 3600 });
      const result = {
        url
      };
      return result;
  } catch(error: any) {
    throw new Error(`Error occured while generating Signed URL ${error.message}`);
  }

};

export const largeFileUpload = async (
  file: Express.Multer.File,
  folder: string
) => {
  const s3Client = new S3Client({
    region: config.aws.awsRegion,
    credentials: {
      accessKeyId: config.aws.awsAccessKeyId,
      secretAccessKey: config.aws.awsSecretAccessKey,
    },
  });

  const bucketName = config.aws.awsBucketName;
  const key = `${folder}/${file.originalname}`;
  const buffer = file.buffer;

  let uploadId;

  try {
    const multipartUpload = await s3Client.send(
      new CreateMultipartUploadCommand({
        Bucket: bucketName,
        Key: key,
        ACL: "public-read",
      })
    );

    uploadId = multipartUpload.UploadId;

    const uploadPromises = [];
    // Multipart uploads require a minimum size of 5 MB per part.
    const partSize = Math.ceil(buffer.length / 5);

    // Upload each part.
    for (let i = 0; i < 5; i++) {
      const start = i * partSize;
      const end = start + partSize;
      uploadPromises.push(
        s3Client
          .send(
            new UploadPartCommand({
              Bucket: bucketName,
              Key: key,
              UploadId: uploadId,
              Body: buffer.subarray(start, end),
              PartNumber: i + 1,
            })
          )
          .then((d) => {
            console.log("Part", i + 1, "uploaded");
            return d;
          })
      );
    }

    const uploadResults = await Promise.all(uploadPromises);

    return await s3Client
      .send(
        new CompleteMultipartUploadCommand({
          Bucket: bucketName,
          Key: key,
          UploadId: uploadId,
          MultipartUpload: {
            Parts: uploadResults.map(({ ETag }, i) => ({
              ETag,
              PartNumber: i + 1,
            })),
          },
        })
      )
      .then(() => {
        return {
          filename: file.originalname,
          url: `https://${config.aws.awsBucketName}.s3.amazonaws.com/${folder}/${file.originalname}`,
        };
      });
  } catch (err) {
    console.error(err);

    if (uploadId) {
      const abortCommand = new AbortMultipartUploadCommand({
        Bucket: bucketName,
        Key: key,
        UploadId: uploadId,
      });

      await s3Client.send(abortCommand);
    }
  }
};
