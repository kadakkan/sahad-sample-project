import { config } from "../config";
import sgMail from "@sendgrid/mail";
import path from "path";
import ejs from "ejs";

sgMail.setApiKey(config.sendgrid_api_key);

export const sendVerificationEmail = async (
  email: string,
  verificationToken: string
) => {
  try {
    const verificationLink = `${
      config.app_url
    }/user/email_verification/${encodeURIComponent(
      email
    )}/${verificationToken}`;
    const templatePath = path.join(
      __dirname,
      "../ejs",
      "email-verification.ejs"
    );

    const emailBody = await ejs.renderFile(templatePath, { verificationLink });
    const msg = {
      to: email,
      from: "kadakkan.sahad@occudiz.com",
      subject: "Email Verification",
      html: emailBody,
    };

    await sgMail.send(msg);
  } catch (error: any) {
    console.log("Error sending verification email.", error);
    throw error;
  }
};

export const forgotPasswordEmail = async (
  email: string,
  verificationToken: string
) => {
  try {
    const verificationLink = `${
      config.front_end_url
    }/reset_forgot_password/${encodeURIComponent(email)}/${verificationToken}`;
    const templatePath = path.join(__dirname, "../ejs", "forgot-password.ejs");

    const emailBody = await ejs.renderFile(templatePath, { verificationLink });
    const msg = {
      to: email,
      from: "kadakkan.sahad@occudiz.com",
      subject: "Reset - Forgot Password",
      html: emailBody,
    };

    await sgMail.send(msg);
  } catch (error: any) {
    console.log("Error sending forgot password email.", error);
    throw error;
  }
};

export const setCredentialsEmail = async (
  email: string,
  verificationToken: string
) => {
  try {
    const verificationLink = `${
      config.front_end_url
    }/setup/${encodeURIComponent(email)}/${verificationToken}`;
    const templatePath = path.join(
      __dirname,
      "../ejs",
      "setup-credentials.ejs"
    );

    const emailBody = await ejs.renderFile(templatePath, { verificationLink });
    const msg = {
      to: email,
      from: "kadakkan.sahad@occudiz.com",
      subject: "Setup - account",
      html: emailBody,
    };

    await sgMail.send(msg);
  } catch (error: any) {
    console.log("Error sending password setup email.", error);
    throw error;
  }
};

export const notificationsEmail = async (
  email: string,
  urlPath: string,
  subject: string,
  message: string
) => {
  try {
    const url = `${config.front_end_url}/${urlPath}`;
    const templatePath = path.join(
      __dirname,
      "../ejs",
      "notifications.ejs"
    );

    const emailBody = await ejs.renderFile(templatePath, { url, message });
    const msg = {
      to: email,
      from: "kadakkan.sahad@occudiz.com",
      subject: subject,
      html: emailBody,
    };

    await sgMail.send(msg);
  } catch (error: any) {
    console.log("Error sending password setup email.", error);
    throw error;
  }
};
