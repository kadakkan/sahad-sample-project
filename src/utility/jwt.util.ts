import jwt from 'jsonwebtoken';
import { IUser, IUserT } from '../models/user.model';
import { config } from '../config';


export const generateToken = (user: IUserT): string => {
  const token = jwt.sign({user: user}, config.jwt_secret!, { expiresIn: '24h' });
  return token;
};


