// server/chatUtility.ts
import { Server, Socket } from 'socket.io';
// import ChatMessage, { IChatMessage } from './models/ChatMessage';

export function setupChat(io: Server) {
  io.on('connection', (socket: Socket) => {
    console.log('a user connected');

    socket.on('disconnect', () => {
      console.log('user disconnected');
    });

    socket.on('chat message', async (msg: string) => {
      console.log('message: ' + msg);
    //   const chatMessage: IChatMessage = new ChatMessage({
    //     sender: 'User', // You can adjust this based on your authentication system
    //     message: msg
    //   });

    //   try {
    //     await chatMessage.save();
        io.emit('chat message', msg);
    //   } catch (err) {
    //     console.error(err);
    //   }
    });
  });
}
