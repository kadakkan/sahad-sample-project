import { config } from "../config";
import { ICard } from "../models/card.model";
import { IOrder } from "../models/order.model";
import userService from "../services/user.service";

const stripe = require("stripe")(config.stripe.secret, {
  apiVersion: config.stripe.api_version,
});

export const addCard = async (customerId: string, token: string) => {
  try {
    const customer = await stripe.customers.retrieve(customerId);

    const card = await stripe.customers.createSource(customerId, {
      source: token,
    });

    return {
      customer,
      card,
    };
  } catch (error: any) {
    console.log("Error adding card.", error.message);
    throw error;
  }
};

export const createPaymentIntent = async (customerId: string, order: IOrder) => {
  try {
    const options = {
      amount: order.total_amount * 100, //fills
      currency: 'aed',
      customer: customerId,
      description: `Payment for order number ${order.order_number}`,
      metadata:{
        order_id: order._id.toString(),
        order_number: order.order_number,
        package: order.package.name,
      },
      // payment_method: defaultPayment.payment_method_id,
      // confirm: true,
      automatic_payment_methods: {
        enabled: true,
        allow_redirects: "always"
      },
      // return_url: config.app_url + "/payment/webhook"
    };

    // console.log(options)
    const paymentIntent = await stripe.paymentIntents.create(options);
    // console.log(paymentIntent);
    return paymentIntent;
  } catch (error: any) {
    console.log("Error creating setup intent.", error.message);
    throw error;
  }
};

export const createSetupIntent = async (customerId: string) => {
  try {
    const setupIntent = await stripe.setupIntents.create({
      customer: customerId,
      payment_method_types: ["card"],
      usage: "on_session",
    });
    return setupIntent.client_secret;
  } catch (error: any) {
    console.log("Error creating payment intent.", error.message);
    throw error;
  }
};

export const getCards = async (customerId: string, token: string) => {
  try {
    const paymentMethods = await stripe.paymentMethods.list({
      customer: customerId,
      type: "card",
    });

    return paymentMethods;
  } catch (error: any) {
    throw error;
  }
};

export const addCustomer = async (name: string, email: string) => {
  try {
    const customer = await stripe.customers.create({ name, email });
    return customer.id;
  } catch (error: any) {
    console.log("Error adding customer.", error);
    throw error;
  }
};

export const saveCard = async (customerId: string, card: any) => {
  try {
    const _card = await stripe.customers.createSource(customerId, {
      source: {
        number: card.number,
        exp_year: card.exp_year,
        exp_month: card.exp_month,
        object: "card",
        name: card.name,
      },
    });

    return _card;
  } catch (error: any) {
    console.log("Error adding customer.", error);
    throw error;
  }
};
