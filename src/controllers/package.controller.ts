import { Request, Response } from "express";
import PackageService from "../services/package.service";

class PackageController {
  async createPackage(req: Request, res: Response): Promise<Response> {
    try {
      const newPackage = req.body;
      const createdPackage = await PackageService.createPackage(newPackage);
      return res.json(createdPackage);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while creating Package." });
    }
  }
  async getPackages(req: Request, res: Response): Promise<Response> {
    try {
      const packages = await PackageService.getAllPackages();
      return res.json(packages);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Packages." });
    }
  }
  async getOtherPackages(req: Request, res: Response): Promise<Response> {
    try {
      const packageId = req.params.id;
      const packages = await PackageService.getOtherPackages(packageId);
      return res.json(packages);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Packages." });
    }
  }
  async getPackageDetail(req: Request, res: Response): Promise<Response> {
    try {
      const packageId = req.params.id;
      const ypackage = await PackageService.getPackageById(packageId);
      if (ypackage) {
        return res.send(ypackage);
      } else {
        return res.status(404).json({ error: "Package does not exist." });
      }
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Package Details." });
    }
  }
  async updatePackage(req: Request, res: Response): Promise<Response> {
    try {
      const packageId = req.params.id;
      const updatedPackageData = req.body;
      const updatedPackage = await PackageService.updatePackage(
        packageId,
        updatedPackageData
      );

      if (updatedPackage) {
        return res.json(updatedPackage);
      } else {
        return res.status(404).json({ error: "Package does not exist." });
      }
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while updating Package." });
    }
  }
  async deletePackage(req: Request, res: Response): Promise<Response> {
    try {
      const packageId = req.params.id;
      const deletedPackage = await PackageService.deletePackage(packageId);

      if (deletedPackage) {
        return res.json(deletedPackage);
      } else {
        return res.status(404).json({ error: "Package does not exist." });
      }
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while deleting Package." });
    }
  }
  async quizSuggestPackage(req: Request, res: Response): Promise<Response> {
    try {
      const params = req.body;
      const packages = await PackageService.getQuizSuggestPackage(params);
      return res.json(packages);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Package suggestion." });
    }
  }
  async updateAddonsForPackages(req: Request, res: Response): Promise<Response> {
    try {
      const addons = req.body;
      const updatedDoc = await PackageService.updateAddonsForPackages(addons);
      return res.json(updatedDoc);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while adding addons." });
    }
  }
}

const packageController = new PackageController();
export default packageController;
