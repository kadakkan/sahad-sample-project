import { Request, Response } from "express";
import GalleryService from "../services/gallery.service";

class GalleryController {
  async createProject(req: Request, res: Response): Promise<Response> {
    try {
      const newProject = req.body;
      const createdProject = await GalleryService.createProject(newProject)
      return res.json(createdProject);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while creating Project." });
    }
  }
  async addImageToGallery(req: Request, res: Response): Promise<Response> {
    try {
      const newImage = req.body;
      const createdGalleryImage = await GalleryService.addImageToGallery(newImage)
      return res.json(createdGalleryImage);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while adding Image to Gallery." });
    }
  }
  async getGallery(req: Request, res: Response): Promise<Response> {
    try {
      const p = req.body;
      const gallery = await GalleryService.getAllGallery(p)
      return res.json(gallery);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Gallery." });
    }
  }
  async singleProjectGallery(req: Request, res: Response): Promise<Response> {
    try {
      const projectId = req.params.id;
      const gallery = await GalleryService.singleProjectGallery(projectId)
      return res.json(gallery);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Project Gallery." });
    }
  }
}

const galleryController = new GalleryController();
export default galleryController;
