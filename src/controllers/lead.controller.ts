import { Request, Response } from "express";
import LeadService from "../services/lead.service";

class LeadController {
  async saveLead(req: Request, res: Response): Promise<Response> {
    try {
      const newLead = req.body;
      const savedLead = await LeadService.saveLead(newLead);
      return res.json(savedLead);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while saving Information." });
    }
  }
  async getLeads(req: Request, res: Response): Promise<Response> {
    try {
      const leads = await LeadService.getAllLeads()
      return res.json(leads);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Leads." });
    }
  }
  async getLeadDetail(req: Request, res: Response): Promise<Response> {
    try {
      const leadId = req.params.id;
      const lead = await LeadService.getLeadById(leadId)
      if (lead) {
        return res.json(lead);
      } else {
        return res.status(404).json({ error: "Lead does not exist." });
      }
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Lead details." });
    }
  }
  async updateLead(req: Request, res: Response): Promise<Response> {
    try {
      const leadId = req.params.id;
      const updatedLeadData = req.body;
      const updatedLead = await LeadService.updateLead(leadId, updatedLeadData);

      if (updatedLead) {
        return res.json(updatedLead);
      } else {
        return res.status(404).json({ error: "Lead does not exist." });
      }
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while updating Lead." });
    }
  }
  async deleteLead(req: Request, res: Response): Promise<Response> {
    try {
      const leadId = req.params.id;
      const deletedLead = await LeadService.deleteLead(leadId);

      if (deletedLead) {
        return res.json(deletedLead);
      } else {
        return res.status(404).json({ error: "Lead does not exist." });
      }
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while deleting Lead." });
    }
  }
}

const leadController = new LeadController();
export default leadController;
