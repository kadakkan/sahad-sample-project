import designerService from "../services/designer.service";
import { Request, Response } from "express";
import userService from "../services/user.service";
import { v4 as uuidv4 } from "uuid";
import { setCredentialsEmail } from "../utility/sendgrid.util";
import { ObjectId } from "mongoose";
import { generateToken } from "../utility/jwt.util";
import { IUser } from "../models/user.model";
import { GetAllOrdersInput } from "../schemas/order";
import { signedUrlUploadFile } from "../utility/s3Upload.util";
import orderService from "../services/order.service";
import ReviewService from "../services/review.service";

class DesignerController {
  async addDesigner(req: Request, res: Response): Promise<Response> {
    try {
      const newDesigner = req.body;
      const createdDesigner = await designerService.addDesigner(newDesigner);
      return res.json(createdDesigner);
    } catch (error: any) {
      return res
        .status(400)
        .send({ error: "Error occured while adding Designer." });
    }
  }

  async getDesigners(req: Request, res: Response): Promise<Response> {
    try {
      const designers = await designerService.getDesigners();
      return res.json(designers);
    } catch (error: any) {
      return res
        .status(400)
        .send({ error: "Error occured while fetching Designers." });
    }
  }

  async designerProfileSwitch(req: Request, res: Response): Promise<Response> {
    try {
      const userId = (req.user as { _id: ObjectId })._id;
      const user = await userService.findUserById1(userId);

      if (!user.is_designer) {
        throw new Error(`Not a designer profile.`);
      }

      let profile_type = "regular";

      if (user.current_profile_type == "regular") {
        profile_type = "designer";
      }

      const designer = await userService.findAndUpdateById(userId, {
        current_profile_type: profile_type,
      });

      const u = {
        _id: user._id,
        full_name: user.full_name,
        email: user.email,
        picture: user.picture,
        role: user.role,
        type: user.type,
        current_profile_type: profile_type,
        is_designer: true,
      };
      const token = generateToken(u);

      return res.json({ token, message: "Profile switched successfully." });
    } catch (error: any) {
      return res.status(400).send({
        error: `Error occured while switching profile: ${error.message}`,
      });
    }
  }

  async approveDesigner(req: Request, res: Response): Promise<Response> {
    try {
      const designerId = req.params.id;
      const designer = await designerService.getDesigner(designerId);

      const { skills, identity_documents, portfolio, dob } = designer;
      const user = await userService.findUserByEmail(designer.email);

      if (!user) {
        const newUser = {
          full_name: designer.name,
          email: designer.email,
          dob: designer.dob,
          role: "regular",
          is_designer: true,
          current_profile_type: "designer",
          designer: {
            order_count: {
              assigned_orders: 0,
              in_progress_orders: 0,
              completed_orders: 0,
            },
            skills: skills,
            identity_documents: identity_documents,
            portfolio: portfolio,
          },
          type: "local",
          password_reset_token: uuidv4(),
          password_reset_token_validity: new Date(
            Date.now() + 3600 * 1000 * 24
          ),
          email_verification: {
            status: true,
            verification_token: uuidv4(),
            verification_token_validity: new Date(
              Date.now() + 3600 * 1000 * 24
            ),
          },
        };

        const newU = await userService.createUser(newUser);
        const emailSent = await setCredentialsEmail(
          newUser.email,
          newUser.password_reset_token
        );
      } else {
        const designerData = {
          dob: dob,
          is_designer: true,
          current_profile_type: "designer",
          designer: {
            order_count: {
              assigned_orders: 0,
              in_progress_orders: 0,
              completed_orders: 0,
            },
            skills: skills,
            identity_documents: identity_documents,
            portfolio: portfolio,
          },
        };
        const updatedUser = await userService.findAndUpdateById(
          user._id,
          designerData
        );
      }
      await designerService.updateDesigner(designer._id, { is_approved: true });
      return res.json({ message: "Designer approved successfully." });
    } catch (error: any) {
      return res
        .status(400)
        .send({ error: "Error occured while approving Designer." });
    }
  }
  async getProfileDetails(req: Request, res: Response): Promise<Response> {
    try {
      const userId = (req.user as { _id: ObjectId })._id;
      const user = await userService.findUserById1(userId);

      const u = {
        _id: user._id,
        full_name: user.full_name,
        current_profile_type: user.current_profile_type,
        is_designer: user.is_designer,
        email: user.email,
        dob: user.dob,
        phone: user.phone,
        allow_promotional_information: user.allow_promotional_information,
        notification_settings: user.notification_settings,
        designer: user.designer,
        picture: user.picture,
      } as IUser;

      return res.send(u);
    } catch (error: any) {
      return res.status(400).send({
        error: `Error occured while fetching Designer profile. ${error.message}`,
      });
    }
  }
  async editProfileDetails(req: Request, res: Response): Promise<Response> {
    try {
      const userId = (req.user as { _id: ObjectId })._id;
      const userData = req.body;
      const User = await userService.updateUser(userId, userData);

      return res.send({ message: "User details updated successfully." });
    } catch (error: any) {
      return res.status(400).send({
        error: `Error occured while editing Designer profile. ${error.message}`,
      });
    }
  }

  async getAllOrders(
    req: Request<{}, {}, GetAllOrdersInput["body"]>,
    res: Response
  ): Promise<Response> {
    try {
      const user = req.user as any;
      const orders = await designerService.getAllOrders(user._id, req.body);
      return res.send(orders);
    } catch (error: any) {
      console.log(error);
      return res.status(400).send({
        message: `Error occured while fetching all orders`,
      });
    }
  }

  async draftmodelSignedUrl(req: Request, res: Response): Promise<Response> {
    try {
      const userId = (req.user as { _id: ObjectId })._id;
      const { order_id, file_name, file_type } = req.body;
      const order = await orderService.getOrderById(order_id);

      if (
        order.assigned_designer?.designer_id.toString() != userId.toString()
      ) {
        throw new Error(`Order not assigned to this designer.`);
      }

      const response = await signedUrlUploadFile(
        file_name,
        "Draftmodel",
        file_type
      );

      if (response instanceof Error) {
        throw new Error("Failed to generate signed URL for S3");
      }

      return res.json(response);
    } catch (error: any) {
      console.log(error);
      return res.status(400).send({
        message: `Error occured while fetching signed url.`,
      });
    }
  }

  async addOrderDraftModel(req: Request, res: Response): Promise<Response> {
    try {
      const userId = (req.user as { _id: ObjectId })._id;
      const { order_id, stage_id, file_name, file_type, url } = req.body;

      const order = await orderService.getOrderById(order_id);

      if (
        order.assigned_designer?.designer_id.toString() != userId.toString()
      ) {
        throw new Error(`Order not assigned to this designer.`);
      }

      const orderStatus = order.order_status;
      let currentStageIndex = orderStatus.findIndex(
        (stage) => stage.stage_id.toString() == stage_id
      );

      if (!orderStatus[currentStageIndex].is_current) {
        throw new Error(`Cannot add draft model at this stage.`);
      }

      const draftModelUpdated = await orderService.updateOrderDraftModel(
        order_id,
        stage_id,
        { file_name, file_type, url }
      );

      return res.send({ message: "Draft model added successfully." });
    } catch (error: any) {
      console.log(error);
      return res.status(400).send({
        message: `Error occured while adding draft model to order.`,
        error: error.message
      });
    }
  }
  async removeOrderDraftModel(req: Request, res: Response): Promise<Response> {
    try {
      const userId = (req.user as { _id: ObjectId })._id;
      const { order_id, stage_id, draft_model_id } = req.body;

      const order = await orderService.getOrderById(order_id);

      if (
        order.assigned_designer?.designer_id.toString() != userId.toString()
      ) {
        throw new Error(`Order not assigned to this designer.`);
      }

      const draftModelRemoved = await orderService.removeOrderDraftModel(
        order_id,
        stage_id,
        draft_model_id
      );

      return res.send({ message: "Draft model removed successfully." });
    } catch (error: any) {
      console.log(error);
      return res.status(400).send({
        message: `Error occured while removing draft model from order.`,
      });
    }
  }
  async getOrderDraftModels(req: Request, res: Response): Promise<Response> {
    try {
      const userId = (req.user as { _id: ObjectId })._id;
      const { order_id, stage_id } = req.params;
      const order = await orderService.getOrderById(order_id);

      // if (
      //   order.assigned_designer?.designer_id.toString() != userId.toString()
      // ) {
      //   throw new Error(`Order not assigned to this designer.`);
      // }

      const draftModels = order.order_status.find(
        (stage) => stage.stage_id.toString() == stage_id.toString()
      )?.draft_model ?? [];

      const draftModelIds = draftModels.map((e) => e.draft_model_id);
      const allReviews = await ReviewService.getReviewWithIds(draftModelIds);
      const fData = draftModels.map((draftModel) => {
        return {
          ...draftModel,
          reviews: allReviews.filter(
            (review) => review.draft_model_id.toString() == draftModel.draft_model_id.toString()
          ),
        };
      });

      return res.send({
        user_id: order.user_id,
        designer_id: order.assigned_designer.designer_id,
        drafts: fData,
      });
    } catch (error: any) {
      console.log(error);
      return res.status(400).send({
        message: `Error occured while fetching draft models from order.`,
      });
    }
  }
  async submitDraftModel(req: Request, res: Response): Promise<Response> {
    try {
      const userId = (req.user as { _id: ObjectId })._id;
      const { order_id, stage_id } = req.body;
      const order = await orderService.getOrderById(order_id);

      if (
        order.assigned_designer?.designer_id.toString() != userId.toString()
      ) {
        throw new Error(`Order not assigned to this designer.`);
      }

      await designerService.submitDraftModel(order_id, stage_id);

      return res.send({ message: "Draft model submitted successfully." });
    } catch (error: any) {
      console.log(error);
      return res.status(400).send({
        message: `Error occured while fetching draft models from order.`,
      });
    }
  }
}
const designerController = new DesignerController();
export default designerController;
