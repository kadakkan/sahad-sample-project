import { Request, Response } from "express";
import BlogService from "../services/blog.service";
class BlogController {
  async createBlog(req: Request, res: Response): Promise<Response> {
    try {
      const newBlog = req.body;
      const createdBlog = await BlogService.createBlog(newBlog)
      return res.json(createdBlog);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while creating Blogs." });
    }
  }
  async getBlogs(req: Request, res: Response): Promise<Response> {
    try {
      const blogs = await BlogService.getBlogs()
      return res.json(blogs);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Blogs." });
    }
  }
  async getAllBlogs(req: Request, res: Response): Promise<Response> {
    try {
      const params = req.body;
      const blogs = await BlogService.getAllBlogs(params)
      return res.json(blogs);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Blogs." });
    }
  }
  async getOtherBlogs(req: Request, res: Response): Promise<Response> {
    try {
      const blogId = req.params.id;
      const blogs = await BlogService.getOtherBlogs(blogId);
      return res.json(blogs);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Blogs." });
    }
  }
  async getBlogsByAuthor(req: Request, res: Response): Promise<Response> {
    try {
      const author = req.body.author;
      const blogs = await BlogService.getBlogsByAuthor(author);
      return res.json(blogs);
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Blogs for the author." });
    }
  }
  async getBlogDetail(req: Request, res: Response): Promise<Response> {
    try {
      const blogId = req.params.id;
      const blog = await BlogService.getBlogById(blogId)
      if (blog) {
        return res.json(blog);
      } else {
        return res.status(404).json({ error: "Blog does not exist." });
      }
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while fetching Blog details." });
    }
  }
  async updateBlog(req: Request, res: Response): Promise<Response> {
    try {
      const blogId = req.params.id;
      const updatedBlogData = req.body;
      const updatedBlog = await BlogService.updateBlog(blogId, updatedBlogData);

      if (updatedBlog) {
        return res.json(updatedBlog);
      } else {
        return res.status(404).json({ error: "Blog does not exist." });
      }
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while updating Blog." });
    }
  }
  async deleteBlog(req: Request, res: Response): Promise<Response> {
    try {
      const blogId = req.params.id;
      const deletedBlog = await BlogService.deleteBlog(blogId);

      if (deletedBlog) {
        return res.json(deletedBlog);
      } else {
        return res.status(404).json({ error: "Blog does not exist." });
      }
    } catch (error: any) {
      return res.status(400).send({ error: "Error occured while deleting Blog." });
    }
  }
}

const blogController = new BlogController();
export default blogController;
