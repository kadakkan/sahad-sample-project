import { Request, Response } from "express";
import { IOrder } from "../models/order.model";
import mongoose, { ObjectId } from "mongoose";
import packageService from "../services/package.service";
import userService from "../services/user.service";
import questionnaireService from "../services/questionnaire.service";
import orderService from "../services/order.service";
import { generateOrderNumber } from "../utility/helper.util";
import stripeController from "./stripe.controller";
import { GetAllOrdersInput } from "../schemas/order";
import { addCustomer, createPaymentIntent, createSetupIntent } from "../utility/stripe.util";
import { OrderStatus } from "../interfaces/order";

class OrderController {
  async createOrder(req: Request, res: Response): Promise<Response> {
    const userId = (req.user as { _id: ObjectId })._id;
    const { package_id, area, add_ons } = req.body;

    try {
      const packageDetails = await packageService.getPackageById(package_id);
      let addOnCost = 0;
      const addOnval = add_ons.map((obj: any) => {
        addOnCost = addOnCost + obj.cost * (obj.quantity ?? 1);
        return {
          _id: obj._id,
          name: obj.name,
          description: obj.description,
          cost: obj.cost,
          quantity: obj.quantity,
        };
      });

      const defaultAddress = await userService.getUserDefaultAddress(userId);
      const packageCost = packageDetails.area
        .filter((item) => item.value === area)
        .reduce((acc, item) => acc + item.cost, 0);

      let totalCost = addOnCost + packageCost;

      const questionnaireData = await questionnaireService.getQuestionnaire();

      const questionnaire = {
        current_section: {
          name: "current_landscape",
          percentage_completed: 0,
        },
        _data: questionnaireData,
      };

      const currentDate = new Date();
      const orderNumber = generateOrderNumber();

      let hasFastDelivery = false;
      let deliveryWeekDistribution = [7, 14, 28, 35, 42]; // 6 weeks

      hasFastDelivery = addOnval.some(
        (addon: any) => addon.name == "fast_delivery",
      );

      if (hasFastDelivery) {
        deliveryWeekDistribution = [7, 14, 21, 28, 35]; // 4 weeks
      }

      const orderEndDate = new Date(
        currentDate.getTime() +
          deliveryWeekDistribution[4] * 24 * 60 * 60 * 1000,
      );

      const orderStatus = [
        {
          stage_id: new mongoose.Types.ObjectId(),
          stage: "Questionnaire",
          stage_name: "questionnaire",
          description: "",
          due_date: currentDate,
          is_current: true,
          status: OrderStatus.PENDING,
        },
        {
          stage_id: new mongoose.Types.ObjectId(),
          stage: "Draft Model",
          stage_name: "draft_model",
          description: "",
          due_date: new Date(
            currentDate.getTime() +
              deliveryWeekDistribution[0] * 24 * 60 * 60 * 1000,
          ),
          is_current: false,
          status: OrderStatus.PENDING,
        },
        {
          stage_id: new mongoose.Types.ObjectId(),
          stage: "Draft Model User Review",
          stage_name: "draft_model_user_review",
          description: "",
          due_date: new Date(
            currentDate.getTime() +
              deliveryWeekDistribution[1] * 24 * 60 * 60 * 1000,
          ),
          is_current: false,
          status: OrderStatus.PENDING,
        },
        {
          stage_id: new mongoose.Types.ObjectId(),
          stage: "Draft 3D Visuals",
          stage_name: "draft_3d_visuals",
          description: "",
          due_date: new Date(
            currentDate.getTime() +
              deliveryWeekDistribution[2] * 24 * 60 * 60 * 1000,
          ),
          is_current: false,
          status: OrderStatus.PENDING,
        },
        {
          stage_id: new mongoose.Types.ObjectId(),
          stage: "Draft 3D Visuals User Review",
          stage_name: "draft_3d_visuals_user_review",
          description: "",
          due_date: new Date(
            currentDate.getTime() +
              deliveryWeekDistribution[3] * 24 * 60 * 60 * 1000,
          ),
          is_current: false,
          status: OrderStatus.PENDING,
        },
        {
          stage_id: new mongoose.Types.ObjectId(),
          stage: "Final Deliverables",
          stage_name: "final_deliverables",
          description: "",
          due_date: new Date(
            currentDate.getTime() +
              deliveryWeekDistribution[4] * 24 * 60 * 60 * 1000,
          ),
          is_current: false,
          status: OrderStatus.PENDING,
        },
      ];

      const order = {
        user_id: userId,
        order_number: orderNumber,
        package: {
          _id: package_id,
          name: packageDetails.name,
          description: packageDetails.description,
          picture: packageDetails.picture,
          cost: packageCost,
          addons: add_ons,
        },
        percentage_completed: 0,
        shipping_address: defaultAddress,
        payment_status:"Pending",
        total_amount: totalCost,
        questionnaire,
        order_start_date: currentDate,
        order_end_date: orderEndDate,
        order_current_status: "questionnaire",
        order_status: orderStatus,
        status: "Pending",
      };

      const newOrder = await orderService.createOrder(order);

      const user = await userService.findUserByIdQ(userId, "stripe_customer_id full_name email");
      if (!user) {
        throw new Error("Could not get user details");
      }
      const { stripe_customer_id, full_name, email } = user;

      let customerId = stripe_customer_id;
      if (!customerId) {
        customerId = await addCustomer(full_name, email);
        await userService.updateUser(userId, { stripe_customer_id: customerId });
      }

      const paymentIntent = await createPaymentIntent(customerId, newOrder);

      return res.status(200).send({ paymentIntent: paymentIntent.client_secret });
    } catch (error: any) {
      console.log(error);
      return res
        .status(400)
        .send({ message: `Error occured while creating order.` });
    }
  }

  async getOrderDetails(req: Request, res: Response): Promise<Response> {
    try {
      const orderId = req.params.id;
      const orderDetails = await orderService.getOrderById(orderId);
      return res.send(orderDetails);
    } catch (error: any) {
      console.log(error);
      return res.status(400).send({
        message: `Error occured while fetching order details`,
      });
    }
  }

  async completeOrderQuestionnaire(
    req: Request,
    res: Response,
  ): Promise<Response> {
    try {
      const { order_id } = req.params;
      const result = await orderService.completeOrderQuestionnaire(order_id);
      return res.send(result);
    } catch (error: any) {
      return res.status(400).send({
        message: `Error occured while completing order questionnaire.`,
      });
    }
  }
  
  async updateOrderQuestionnaire(
    req: Request,
    res: Response,
  ): Promise<Response> {
    try {
      const { order_id, questionnaire } = req.body;
      const orderDetails = await orderService.updateOrderQuestionnaire(
        order_id,
        questionnaire,
      );
      return res.send(orderDetails);
    } catch (error: any) {
      console.log(error);
      return res.status(400).send({
        message: `Error occured while updating order questionnaire.`,
      });
    }
  }

  async getAllOrders(
    req: Request<{}, {}, GetAllOrdersInput["body"]>,
    res: Response,
  ): Promise<Response> {
    try {
      const userId = (req.user as { _id: ObjectId })._id;
      const orders = await orderService.getAllOrders(userId, req.body);
      return res.send(orders);
    } catch (error: any) {
      console.log(error);
      return res.status(400).send({
        message: `Error occured while fetching all orders`,
      });
    }
  }
}


const orderController = new OrderController();
export default orderController;
