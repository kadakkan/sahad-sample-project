import { Request, Response, NextFunction } from "express";
import passport from "passport";
import AuthService from "../services/auth.service";
import { generateToken } from "../utility/jwt.util";
import { IUser, IUserT } from "../models/user.model";
import { sendVerificationEmail } from "../utility/sendgrid.util";
import { v4 as uuidv4 } from "uuid";

class AuthController {
  async register(req: Request, res: Response): Promise<Response> {
    const { email, password, full_name, country } = req.body;
    
    try {
      const existingUser = await AuthService.findUserByEmail(email);
  
      if (existingUser) {
        return res.status(400).json({ error: "User already exists" });
      }
      const verificationToken = uuidv4();
      const verificationTokenValidity = new Date(Date.now() + 3600 * 1000 * 24);
      // await sendVerificationEmail(email, verificationToken);
      const userData = {
        email,
        password,
        full_name,
        country,
        email_verification: {
          status: false,
          verification_token: verificationToken,
          verification_token_validity: verificationTokenValidity,
        },
        type:'local'
      };
      const newUser = await AuthService.createUser(userData);
      const token = generateToken(newUser);
      return res
        .status(201)
        .json({ message: "User created successfully", token });
    } catch (error) {
      console.error("Error registering user:", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

  async login(req: Request, res: Response, next: NextFunction) {
    passport.authenticate("local", (err: any, user: IUserT) => {
      if (err) {
        return next(err);
      }

      if (!user) {
        return res
          .status(401)
          .json({ error: "Invalid Credentials, Email or password incorrect!" });
      }

      req.login(user, { session: false }, (err) => {
        if (err) {
          return next(err);
        }
        const token = generateToken(user);
        return res.json({ token, ...user });
      });
    })(req, res, next);
  }
}

const authController = new AuthController();
export default authController;
