import passport from "passport";
import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as FacebookStrategy } from "passport-facebook";
import { Strategy as GoogleStrategy } from "passport-google-oauth20";
import AuthService from "../services/auth.service";
import { comparePassword } from "../utility/password.util";
import { config } from "../config";
import { IUserT } from "../models/user.model";

passport.use(
  new LocalStrategy(
    { usernameField: "email" },
    async (email, password, done) => {
      try {
        let user = await AuthService.findUserByEmail(email);

        if (!user) {
          return done(null, false);
        }

        const isValidPassword = await comparePassword(
          password,
          user.password || ""
        );

        if (!isValidPassword) {
          return done(null, false);
        }

        const u = {
          _id: user._id,
          full_name: user.full_name,
          email: user.email,
          picture: user.picture,
          role: user.role,
          type: user.type,
          current_profile_type: user.current_profile_type || "regular",
          is_designer: user.is_designer || false
        } as IUserT;

        return done(null, u);
      } catch (error) {
        return done(error);
      }
    }
  )
);

passport.use(
  new FacebookStrategy(
    {
      clientID: config.facebook.client_id,
      clientSecret: config.facebook.client_secret,
      callbackURL: config.facebook.callback_url,
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        console.log("facebook profile", profile);
        let user = await AuthService.findUserBySocialId(profile.id);
        if (!user) {
          user = await AuthService.createUserSocial({
            social: {
              id: profile.id,
              media: "facebook",
              token: accessToken,
              refresh_token: refreshToken,
            },
            full_name: profile.displayName,
            type: "social",
            //   picture: profile.photos
          });
        }
        return done(null, user);
      } catch (error) {
        return done(error);
      }
    }
  )
);

passport.use(
  new GoogleStrategy(
    {
      clientID: config.google.client_id,
      clientSecret: config.google.client_secret,
      callbackURL: config.google.callback_url,
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const _user = {
          social: {
            id: profile.id,
            media: "google",
            token: accessToken,
            refresh_token: refreshToken,
          },
          type: "social",
          email: profile._json?.email || "",
          full_name: profile.displayName,
          picture: profile._json?.picture || "",
        };

        let user = await AuthService.findUserByEmail(profile._json?.email!);

        if (!user) {
          user = await AuthService.createUserSocial(_user);
          return done();
        }
        return done(null, user);
      } catch (error: any) {
        return done(error);
      }
    }
  )
);

passport.serializeUser((user: any, done) => {
  done(null, user._id);
});

passport.deserializeUser((id: string, done) => {
  const user = AuthService.findUserById(id);
  done(null, user);
});
