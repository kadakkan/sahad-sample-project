import express, { Response, Request } from "express";
import blogController from "../controllers/blog.controller";
import {
  isAuthenticated,
  isAuthenticatedAsAdmin,
} from "../middlewares/auth.middleware";
const router = express.Router();

router.post("/", isAuthenticatedAsAdmin, async (req: Request, res: Response) =>
  blogController.createBlog(req, res)
);
router.get("/:id", async (req: Request, res: Response) =>
  blogController.getBlogDetail(req, res)
);
router.put(
  "/:id",
  isAuthenticatedAsAdmin,
  async (req: Request, res: Response) => blogController.updateBlog(req, res)
);
router.delete(
  "/:id",
  isAuthenticatedAsAdmin,
  async (req: Request, res: Response) => blogController.deleteBlog(req, res)
);
router.get("/get_other_blogs/:id", async (req: Request, res: Response) =>
  blogController.getOtherBlogs(req, res)
);
router.post("/get_blogs_by_author", async (req: Request, res: Response) =>
  blogController.getBlogsByAuthor(req, res)
);
router.get("/", async (req: Request, res: Response) =>
  blogController.getBlogs(req, res)
);
router.post("/all", async (req: Request, res: Response) =>
  blogController.getAllBlogs(req, res)
);

export default router;
