import passport from "passport";
import express, { Response, Request, NextFunction } from "express";
import { isAuthenticated, isLoggedIn } from "../middlewares/auth.middleware";
import authController from "../controllers/auth.controller";
import validate from "../middlewares/validate.middleware";
import { userLoginSchema, userRegisterSchema } from "../schemas/user";
import { config } from "../config";
import { generateToken } from "../utility/jwt.util";
import { IUser, IUserT } from "../models/user.model";

const router = express.Router();

router.post("/login", async (req: Request, res: Response, next: NextFunction) =>
  authController.login(req, res, next)
);
router.post(
  "/register",
  validate(userRegisterSchema),
  async (req: Request, res: Response) => authController.register(req, res)
);

router.get("/user", isAuthenticated, async (req: Request, res: Response) => {
  res.send({ user: req.user });
});

router.get(
  "/google",
  passport.authenticate("google", { scope: ["profile", "email"] }),
  async (req: Request, res: Response) => res.send({ user: req.user })
);

router.get(
  "/google/callback",
  passport.authenticate("google", {
    failureRedirect:
      config.front_end_url + '/login?error_message="Authentication failed"',
  }),
  (req, res) => {
    const user: IUserT = req.user as IUserT;
    const token = generateToken(user);
    return res.json({ token });
  }
);

router.get(
  "/facebook",
  passport.authenticate("facebook", { scope: ["public_profile"] }),
  async (req: Request, res: Response) => res.send({ user: req.user })
);

router.get(
  "/facebook/callback/",
  passport.authenticate("facebook", {
    failureRedirect:
      config.front_end_url + '/login?error_message="Authentication failed"',
  }),
  (req, res) => {
    const user: IUserT = req.user as IUserT;
    const token = generateToken(user);
    return res.json({ token });
  }
);

export default router;
