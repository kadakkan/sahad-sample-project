import express, { Response, Request } from "express";
import designerController from "../controllers/designer.controller";
import validate from "../middlewares/validate.middleware";
import { designerSchema, draftModelAddSchema, draftModelGetSchema, draftModelRemoveSchema, submitDraftModelSchema } from "../schemas/designer";
import verifyRecaptcha from "../middlewares/recaptcha.middleware";
import {
  isAuthenticated,
  isAuthenticatedAsAdmin,
} from "../middlewares/auth.middleware";
import { designerUserSchema } from "../schemas/user";
import { getAllOrdersSchema } from "../schemas/order";
import { draftModelUploadSchema } from "../schemas/upload";
import { signedUrlUploadFile } from "../utility/s3Upload.util";
import { isDesigner } from "../middlewares/designer.middleware";
import orderController from "../controllers/order.controller";
const router = express.Router();

router.post(
  "/",
  [
    validate(designerSchema),
    // verifyRecaptcha
  ],
  async (req: Request, res: Response) =>
    designerController.addDesigner(req, res)
);

router.get("/", isAuthenticatedAsAdmin, async (req: Request, res: Response) =>
  designerController.getDesigners(req, res)
);
router.put(
  "/approve_designer/:id",
  isAuthenticatedAsAdmin,
  async (req: Request, res: Response) =>
    designerController.approveDesigner(req, res)
);
router.post(
  "/designer_profile_switch",
  isAuthenticated,
  async (req: Request, res: Response) =>
    designerController.designerProfileSwitch(req, res)
);
router.get(
  "/get_profile_details",
  [isAuthenticated, isDesigner],
  async (req: Request, res: Response) =>
    designerController.getProfileDetails(req, res)
);
router.post(
  "/edit_profile_details",
  [isAuthenticated, isDesigner, validate(designerUserSchema)],
  async (req: Request, res: Response) =>
    designerController.editProfileDetails(req, res)
);
router.post(
  "/get_all_orders",
  [isAuthenticated, isDesigner, validate(getAllOrdersSchema)],
  async (req: Request, res: Response) =>
    designerController.getAllOrders(req, res)
);

router.post(
  "/draftmodel_signed_url",
  [isAuthenticated, isDesigner, validate(draftModelUploadSchema)],
  async (req: Request, res: Response) =>
    designerController.draftmodelSignedUrl(req, res)
);

router.post(
  "/add_order_draftmodel",
  [isAuthenticated, isDesigner, validate(draftModelAddSchema)],
  async (req: Request, res: Response) =>
    designerController.addOrderDraftModel(req, res)
);

router.delete(
  "/remove_order_draftmodel",
  [isAuthenticated, isDesigner, validate(draftModelRemoveSchema)],
  async (req: Request, res: Response) =>
    designerController.removeOrderDraftModel(req, res)
);

router.get(
  "/get_order_draftmodels/:order_id/:stage_id",
  [isAuthenticated, validate(draftModelGetSchema)],
  async (req: Request, res: Response) =>
    designerController.getOrderDraftModels(req, res)
);

router.post(
  "/submit_draftmodel",
  [isAuthenticated, isDesigner, validate(submitDraftModelSchema)],
  async (req: Request, res: Response) =>
    designerController.submitDraftModel(req, res)
);

export default router;
