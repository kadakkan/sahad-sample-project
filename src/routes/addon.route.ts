import express, { Response, Request } from "express";
import packageController from "../controllers/package.controller";
import validate from "../middlewares/validate.middleware";
import { addonsSchema } from "../schemas/addons";
import {
  isAuthenticated,
  isAuthenticatedAsAdmin,
} from "../middlewares/auth.middleware";
const router = express.Router();

router.post(
  "/update_addons_for_packages",
  [validate(addonsSchema), isAuthenticatedAsAdmin],
  async (req: Request, res: Response) =>
    packageController.updateAddonsForPackages(req, res)
);
export default router;
