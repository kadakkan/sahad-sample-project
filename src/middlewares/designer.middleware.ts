import { Request, Response, NextFunction } from "express";

export const isDesigner = (req: Request, res: Response, next: NextFunction) => {
  const is_designer = (req.user as { is_designer: Boolean }).is_designer;
  if (is_designer) {
    next();
  } else
    return res
      .status(401)
      .json({ error: "Not designer previlages found for user." });
};
