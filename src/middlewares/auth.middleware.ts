import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { config } from "../config";
import userService from "../services/user.service";

export const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.status(401).json({ error: "Unauthorized" });
};

export const isAuthenticated = (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const token = req.headers["authorization"];

  if (!token) {
    return res.status(401).json({ error: "Unauthorized" });
  }

  jwt.verify(token, config.jwt_secret, async (err: any, decoded: any) => {
    if (err) {
      return res.sendStatus(403);
    }
    try {
      const user = await userService.findUserById(decoded.user._id);
      if (!user) {
        return res.status(404).json({ error: "User does not exist." });
      }
      req.user = user;
      next();
    } catch (error) {
      console.error("Error verifying token:", error);
      return res
        .status(500)
        .json({ error: "Error occured while verifying token" });
    }
  });
};

export const isAuthenticatedAsAdmin = (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const token = req.headers["authorization"];

  if (!token) {
    return res.status(401).json({ error: "Unauthorized" });
  }

  jwt.verify(token, config.jwt_secret, async (err: any, decoded: any) => {
    if (err) {
      return res.sendStatus(403);
    }
    try {
      const user = await userService.findAdminById(decoded.user._id);
      if (!user) {
        return res.status(404).json({
          error: "User does not exist or does not have Admin privilages.",
        });
      }
      req.user = user;
      next();
    } catch (error) {
      console.error("Error verifying token:", error);
      return res
        .status(500)
        .json({ error: "Error occured while verifying token" });
    }
  });
};
