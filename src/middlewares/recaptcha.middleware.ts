// recaptchaMiddleware.ts
import { Request, Response, NextFunction } from 'express';
import axios from 'axios';
import { config } from '../config';

const verifyRecaptcha = async (req: Request, res: Response, next: NextFunction) => {
    const token = req.body.recaptchaToken;

    try {
        const response = await axios.post(
            `https://www.google.com/recaptcha/api/siteverify`,
            null,
            {
                params: {
                    secret: config.recaptcha.secretKey,
                    response: token
                }
            }
        );

        const { success } = response.data;

        if (success) {
            next();
        } else {
            return res.status(403).json({ error: 'reCAPTCHA verification failed' });
        }
    } catch (error) {
        console.error('Error verifying reCAPTCHA', error);
        return res.status(500).json({ error: 'Internal server error' });
    }
};

export default verifyRecaptcha;
