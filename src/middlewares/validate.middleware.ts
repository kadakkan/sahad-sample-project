import { Request, Response, NextFunction } from "express";

export function parseDate(value: string): Date | undefined {
  const parsedDate = new Date(value);
  return isNaN(parsedDate.getTime()) ? undefined : parsedDate;
}

const validate =
  (schema: any) => (req: Request, res: Response, next: NextFunction) => {
    try {
      schema.parse({
        body: req.body,
        query: req.query,
        params: req.params,
      });
      next();
    } catch (error: any) {
      res
        .status(400)
        .json({ message: "Invalid Input", error: error.errors[0].message });
    }
  };
export default validate;
