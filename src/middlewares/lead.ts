import { z } from "zod";
import { Request, Response, NextFunction } from "express";

const GINleadSchema = z.object({
  email: z.string().email(),
  full_name: z.string().max(100).min(2).optional(),
  phone_number: z.string().max(15).min(10).optional(),
  country: z.string().optional(),
  request_info: z.string().max(500).optional(),
  location: z
    .object({
      latitude: z.number(),
      longitude: z.number(),
      address: z.string(),
    })
    .optional(),
});

const QleadSchema = z.object({
  email: z.string().email(),
  full_name: z.string().max(100).min(2).optional(),
  phone_number: z.string().max(20).min(10).optional(),
  country: z.string().optional().optional(),
  request_info: z.string().max(500).optional().optional(),
  location: z
    .object({
      latitude: z.number(),
      longitude: z.number(),
      address: z.string(),
    })
    .optional(),
});

const LPleadSchema = z.object({
  email: z.string().email(),
  full_name: z.string().max(100).min(2).optional(),
  phone_number: z.string().max(20).min(10).optional(),
  country: z.string().optional(),
  request_info: z.string().max(500).optional(),
  location: z
    .object({
      latitude: z.number(),
      longitude: z.number(),
      address: z.string(),
    })
    .optional(),
});

function validateRequestBodyGINLead(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    GINleadSchema.parse(req.body);
    next();
  } catch (error: any) {
    res.status(400).json({ error: error.errors[0].message });
  }
}

function validateRequestBodyQLead(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    QleadSchema.parse(req.body);
    next();
  } catch (error: any) {
    res.status(400).json({ error: error.errors[0].message });
  }
}

function validateRequestBodyLPLead(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    LPleadSchema.parse(req.body);
    next();
  } catch (error: any) {
    res.status(400).json({ error: error.errors[0].message });
  }
}

export {
  validateRequestBodyGINLead,
  validateRequestBodyQLead,
  validateRequestBodyLPLead,
};
