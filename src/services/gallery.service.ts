import { GalleryModel, IGallery } from "../models/gallery.model";
import { ProjectModel, IProject } from "../models/project.model";

class GalleryService {
   async createProject(projectData: IProject): Promise<IProject> {
    try {
      const createdProject = await ProjectModel.create(projectData);
      return createdProject;
    } catch (error: any) {
      throw new Error(`Error creating project: ${error.message}`);
    }
  }

   async addImageToGallery(galleryData: IGallery): Promise<IGallery> {
    try {
      const createdGallery = await GalleryModel.create(galleryData);
      return createdGallery;
    } catch (error: any) {
      throw new Error(`Error creating gallery: ${error.message}`);
    }
  }

   async getAllGallery(pdata: { page: number, limit: number }): Promise<{ gallery: IGallery[]; count: number }> {
    try {
      const limit = pdata.limit ?? 12;
      const page = pdata.page ?? 0;

      const gallery = await GalleryModel.find()
        .populate({
          path: "project",
          populate: { path: "package", select: "name description" },
          select: "name description key_points package",
        })
        .skip(page * limit)
        .limit(limit);
      const count = await GalleryModel.countDocuments();
      return {
        gallery: gallery,
        count: count,
      };
    } catch (error: any) {
      throw new Error(`Error fetching gallery: ${error.message}`);
    }
  }

   async singleProjectGallery(projectId: string): Promise<any> {
    try {
      const [project, gallery] = await Promise.all([
        ProjectModel.findOne({ _id: projectId }).populate("package").lean(),
        GalleryModel.find({ project: projectId }),
      ]);
      if (!project) {
        throw new Error("Project not found");
      }
      project.galleryImages = gallery;

      return project;
    } catch (error: any) {
      throw new Error(`Error fetching gallery: ${error.message}`);
    }
  }
}

const galleryService = new GalleryService();
export default galleryService;
