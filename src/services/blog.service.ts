import { BlogModel, IBlog } from '../models/blog.model';

class BlogService {
     async createBlog(blogData: IBlog): Promise<IBlog> {
      try {
        const createdBlog = await BlogModel.create(blogData);
        return createdBlog;  
      } catch (error:any) {
        throw new Error(`Error creating blog: ${error.message}`);
      }
    }
  
     async getBlogs(): Promise<IBlog[]> {
      try {
        const blogs = await BlogModel.find().limit(4);
        return blogs;
      } catch (error:any) {
        throw new Error(`Error fetching blogs: ${error.message}`);
      }
    }

     async getAllBlogs(pdata: { limit: number, page:number }): Promise<{ blogs: IBlog[], count: number }> {
      try {
        const limit = pdata.limit ?? 12;
        const page = pdata.page ?? 0;
        
        const [ allblogs , count] = await Promise.all([await BlogModel.find().skip(page * limit).limit(limit).lean(), 
          BlogModel.countDocuments()]);
        return {
         blogs:  allblogs,
         count: count
        };
      } catch (error:any) {
        throw new Error(`Error fetching blogs: ${error.message}`);
      }
    }

     async getOtherBlogs(blogId: string): Promise<IBlog[]> {
      try {
        const otherblog = await BlogModel.find({ _id:{$nin: blogId}}).limit(4);
        return otherblog;
      } catch (error:any) {
        throw new Error(`Error fetching other blogs: ${error.message}`);
      }
    }

     async getBlogsByAuthor(author: string): Promise<IBlog[]> {
      try {
        const blogsbyauthor = await BlogModel.find({ author:author});
        return blogsbyauthor;
      } catch (error:any) {
        throw new Error(`Error fetching other blogs from author: ${error.message}`);
      }
    }
  
     async getBlogById(blogId: string): Promise<IBlog | null> {
      try {
        const blog = await BlogModel.findById(blogId);
        return blog;
      } catch (error:any) {
        throw new Error(`Error fetching blog by ID: ${error.message}`);
      }
    }
  
     async updateBlog(blogId: string, updatedBlogData: Partial<IBlog>): Promise<IBlog | null> {
      try {
        const updatedBlog = await BlogModel.findByIdAndUpdate(blogId, updatedBlogData, { new: true });
        return updatedBlog;
      } catch (error:any) {
        throw new Error(`Error updating blog: ${error.message}`);
      }
    }
  
     async deleteBlog(blogId: string): Promise<IBlog | null> {
      try {
        const deletedBlog = await BlogModel.findByIdAndDelete(blogId);
        return deletedBlog;
      } catch (error:any) {
        throw new Error(`Error deleting blog: ${error.message}`);
      }
    }
  }
  
  const blogService = new BlogService();
  export default blogService;