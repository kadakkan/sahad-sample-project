import { LeadModel, ILead } from "../models/lead.model";

class LeadService {
   async createLead(leadData: ILead): Promise<ILead> {
    try {
      const createdLead = await LeadModel.create(leadData);
      return createdLead;
    } catch (error: any) {
      throw new Error(`Error creating lead: ${error.message}`);
    }
  }
   async saveLead(leadData: ILead): Promise<ILead> {
    try {
      const { email } = leadData;
      const createdLead = await LeadModel.findOneAndUpdate(
        { email: email },
        leadData,
        { upsert: true, new: true, setDefaultsOnInsert: true }
      );
      return createdLead;
    } catch (error: any) {
      throw new Error(`Error creating lead: ${error.message}`);
    }
  }
   async getLeadByEmail(email: string): Promise<ILead | null> {
    try {
      const leads = await LeadModel.findOne({ email: email });
      return leads;
    } catch (error: any) {
      throw new Error(`Error creating lead: ${error.message}`);
    }
  }

   async getAllLeads(): Promise<ILead[]> {
    try {
      const leads = await LeadModel.find();
      return leads;
    } catch (error: any) {
      throw new Error(`Error fetching leads: ${error.message}`);
    }
  }

   async getLeadById(leadId: string): Promise<ILead | null> {
    try {
      const lead = await LeadModel.findById(leadId);
      return lead;
    } catch (error: any) {
      throw new Error(`Error fetching lead by ID: ${error.message}`);
    }
  }

   async updateLead(
    leadId: string,
    updatedLeadData: Partial<ILead>
  ): Promise<ILead | null> {
    try {
      const updatedLead = await LeadModel.findByIdAndUpdate(
        leadId,
        updatedLeadData,
        { new: true }
      );
      return updatedLead;
    } catch (error: any) {
      throw new Error(`Error updating lead: ${error.message}`);
    }
  }

   async deleteLead(leadId: string): Promise<ILead | null> {
    try {
      const deletedLead = await LeadModel.findByIdAndDelete(leadId);
      return deletedLead;
    } catch (error: any) {
      throw new Error(`Error deleting lead: ${error.message}`);
    }
  }
}

const leadService = new LeadService();
export default leadService;
