import { UserModel, IUser, IUserT } from "../models/user.model";
import { hashPassword } from "../utility/password.util";

class AuthService {
  async findUserByEmail(email: string): Promise<IUser | null> {
    try {
      const user = await UserModel.findOne({ email: email }).lean();
      return user;
    } catch (error: any) {
      throw new Error(`Error fetching user by email: ${error.message}`);
    }
  }
  async findUserById(id: string): Promise<IUser | null> {
    try {
      const user = await UserModel.findById(id).lean();
      return user;
    } catch (error: any) {
      throw new Error(`Error fetching user by ID: ${error.message}`);
    }
  }

  async findUserBySocialId(id: string): Promise<IUser | null> {
    try {
      const user = await UserModel.findOne({ social_id: id }).lean();
      return user;
    } catch (error: any) {
      throw new Error(`Error fetching user by social ID: ${error.message}`);
    }
  }

  async createUser(userData: Partial<IUser>): Promise<IUserT> {
    try {
      userData.password = await hashPassword(userData.password || "");
      const newUser = await UserModel.create(userData);

      const user = {
        _id: newUser._id,
        full_name: newUser.full_name,
        email: newUser.email,
        picture: newUser.picture,
        role: newUser.role,
        type: newUser.type,
        current_profile_type: newUser.current_profile_type || "regular",
        is_designer: newUser.is_designer || false
      };

      return user;
    } catch (error: any) {
      throw new Error(`Error creating user: ${error.message}`);
    }
  }

  async createUserSocial(userData: Partial<IUser>): Promise<IUser> {
    try {
      const newUser = await UserModel.create(userData);
      return newUser;
    } catch (error: any) {
      throw new Error(`Error creating user: ${error.message}`);
    }
  }
}

const authService = new AuthService();
export default authService;
