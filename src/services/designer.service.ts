import mongoose, { ObjectId, Schema } from "mongoose";
import { DesignerModel, IDesigner } from "../models/designer.model";
import { GetAllOrdersInput } from "../schemas/order";
import { IOrder, OrderModel } from "../models/order.model";
import { OrderStatus } from "../interfaces/order";

class DesignerService {
  async addDesigner(designerData: IDesigner): Promise<IDesigner> {
    try {
      const emailExist = await DesignerModel.findOne({
        email: designerData.email,
      });
      if (emailExist) throw new Error("Email already exists");
      const createdDesigner = await DesignerModel.create(designerData);
      return createdDesigner;
    } catch (error: any) {
      throw new Error(`Error creating designer: ${error.message}`);
    }
  }

  async getDesigners(): Promise<IDesigner[]> {
    try {
      const designers = await DesignerModel.find().lean();
      return designers;
    } catch (error: any) {
      throw new Error(`Error fetching designers: ${error.message}`);
    }
  }

  async updateDesigner(
    designerId: string,
    designerData: Partial<IDesigner>,
  ): Promise<IDesigner> {
    try {
      const designer = await DesignerModel.findByIdAndUpdate(
        designerId,
        designerData,
      );

      if (!designer) {
        throw new Error("Invalid designer ID.");
      }

      return designer;
    } catch (error: any) {
      throw new Error(`Error fetching designers: ${error.message}`);
    }
  }

  async getDesigner(designerId: string): Promise<IDesigner> {
    try {
      const d = await DesignerModel.findById(designerId).lean();

      if (!d) {
        throw new Error(`Designer does not exists.`);
      }
      return d;
    } catch (error: any) {
      throw new Error(`Error fetching designers: ${error.message}`);
    }
  }

  async getAllOrders(
    userId: ObjectId,
    pdata: GetAllOrdersInput["body"],
  ): Promise<{ orders: IOrder[]; count: number }> {
    const { page = 0, limit = 10, status, search } = pdata;
    const query = {
      "assigned_designer.designer_id": userId,
      ...(status === OrderStatus.COMPLETED && {
        status: OrderStatus.COMPLETED,
      }),
      ...(status !== OrderStatus.COMPLETED && {
        status: { $ne: OrderStatus.COMPLETED },
      }),
      ...(search && {
        $or: [
          { order_number: { $regex: search, $options: "i" } },
          { "package.name": { $regex: search, $options: "i" } },
        ],
      }),
    };
    const orders = await OrderModel.find(query)
      .select(
        "_id order_number total_amount assigned_designer status package add_ons order_start_date order_end_date createdAt",
      )
      .skip(page * limit)
      .limit(limit);
    const count = await OrderModel.countDocuments(query);
    return {
      orders,
      count,
    };
  }

  async submitDraftModel(orderId: string, stageId: string): Promise<boolean> {
    try {
      const order = await OrderModel.findOne({
        _id: new mongoose.Types.ObjectId(orderId),
      });

      if (!order) {
        throw new Error(`Invalid order ID.`);
      }

      const order_status = order.order_status;
      let index = order_status.findIndex(
        (status) => status.stage_name === "draft_model"
      );

      if(order_status[index].is_current === false) {
        throw new Error(`Cannot submit draft model at this stage.`)
      }

      order_status[index].is_current = false;
      order_status[index].status = OrderStatus.COMPLETED;
      order_status[index].submitted_date = new Date();
      const draftModel = order_status[index].draft_model;
      index += 1;
      order_status[index].is_current = true;
      order_status[index].status = OrderStatus.INPROGRESS;
      order_status[index].draft_model = draftModel;
      await OrderModel.updateOne(
        { _id: new mongoose.Types.ObjectId(orderId) },
        { order_status, percentage_completed: 32 }
      );
      return true;
    } catch (error: any) {
      throw new Error(`Error occured while submitting draft model. ${error.message}`);
    }
  }
}

const designerService = new DesignerService();
export default designerService;
