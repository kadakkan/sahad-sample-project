export enum OrderStatus {
  PENDING = "pending",
  INPROGRESS = "in_progress",
  COMPLETED = "completed",
  CANCELLED = "cancelled",
}
