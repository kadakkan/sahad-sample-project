const app_url = "http://localhost:3000/api";
const front_end_url = "http://locahost:4001";

export const config = {
  port: 3000,
  app_url,
  front_end_url,
};
