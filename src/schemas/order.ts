import { z } from "zod";
import { ObjectId } from "mongodb";
import { OrderStatus } from "../interfaces/order";

export const orderSchema = z.object({
  body: z.object({
    package_id: z.string(),
    add_ons: z.array(
      z
        .object({
          _id: z.string(),
          name: z.string().max(100).min(2),
          label: z.string().max(100).min(2),
          description: z.string().max(500),
          has_quantity: z.boolean(),
          quantity: z.number().optional(),
          picture: z.string().optional(),
          cost: z.number(),
        })
        .optional()
    ),
  }),
});

export const getAllOrdersSchema = z.object({
  body: z.object({
    page: z.number().optional(),
    limit: z.number().optional(),
    search: z.string().optional(),
    status: z.enum(["", ...Object.values(OrderStatus)]).optional(),
  }),
});
export type GetAllOrdersInput = z.infer<typeof getAllOrdersSchema>;

export const completeOrderSchema = z.object({
  params: z.object({
    order_id: z.string(),
  }),
});

export const updateOrderQSchema = z.object({
  body: z.object({
    order_id: z.string(),
    questionnaire: z.object({}),
  }),
});

export const checkoutOrderSchema = z.object({
  body: z.object({
    order_id: z.string(),
  }),
});
