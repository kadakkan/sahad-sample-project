import { z } from "zod";

export const referSchema = z.object({
  body: z.object({
    name: z.string().max(100).min(2),
    email: z.string().email(),
  }),
});
