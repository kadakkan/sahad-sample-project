import { z } from "zod";

export const addReviewSchema = z.object({
  body: z.object({
    draft_model_id: z.string(),
    review_type: z.enum(["like", "dislike", "comment"]),
    position: z.object({
      x: z.number(),
      y: z.number()
    }),
    comment: z.string(),
  }),
});

export const editReviewSchema = z.object({
  body: z.object({
    review_type: z.enum(["like", "dislike", "comment"]),
    comment: z.string().optional(),
  }),
  params: z.object({
    review_id: z.string(),
  }),
}); 

export const allReviewsSchema = z.object({
  body: z.object({
    page: z.number().optional(),
    limit: z.number().optional(),
  }),
  params: z.object({
    draft_model_id: z.string(),
  }),
});
export type GetAllReviewInput = z.infer<typeof allReviewsSchema>;


export const addReplySchema = z.object({
  body: z.object({
    review_id: z.string(),
    reply: z.string(),
  }),
});
