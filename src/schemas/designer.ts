import { z } from "zod";
import { parseDate } from "../middlewares/validate.middleware";

export const designerSchema = z.object({
  body: z.object({
    name: z.string().max(100).min(2),
    dob: z
      .string()
      .refine((value) => parseDate(value) !== undefined, {
        message: "Invalid date format",
      })
      .transform(parseDate),
    email: z.string().email(),
    skills: z.array(
      z.object({
        skill_name: z.string(),
        level: z.number().max(5).min(1),
      })
    ),
    identity_documents: z.array(
      z.object({
        document_name: z.string(),
        url: z.string().url(),
      })
    ),
    portfolio: z.array(
      z.object({
        project_name: z.string(),
        url: z.string().url(),
      })
    ),
  }),
});

export const draftModelAddSchema = z.object({
  body: z.object({
    url: z.string().url(),
    file_name: z.string(),
    file_type: z.string(),
    order_id: z.string(),
    stage_id: z.string()
  }),
});

export const draftModelRemoveSchema = z.object({
  body: z.object({
    order_id: z.string(),
    stage_id: z.string(),
    draft_model_id: z.string()
  }),
});

export const draftModelGetSchema = z.object({
  params: z.object({
    order_id: z.string(),
    stage_id: z.string(),
  }),
});

export const submitDraftModelSchema = z.object({
  body: z.object({
    order_id: z.string(),
    stage_id: z.string(),
  }),
});
