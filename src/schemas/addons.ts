import { z } from "zod";

export const addonsSchema = z.object({
  body: z.array(
    z.object({
      name: z.string().max(100).min(2),
      label: z.string().max(100).min(2),
      description: z.string().max(500),
      has_quantity: z.boolean(),
      quantity: z.number().optional(),
      picture: z.string().optional(),
      cost: z.number(),
    })
  ),
});
