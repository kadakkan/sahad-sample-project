import { string, z } from "zod";
import { ObjectId } from "mongodb";
import { parseDate } from "../middlewares/validate.middleware";

export const userLoginSchema = z.object({
  body: z.object({
    full_name: z.string().max(100).min(2),
    email: z.string().email(),
    password: z.string().min(6),
  }),
});

export const userRegisterSchema = z.object({
  body: z
    .object({
      full_name: z.string().max(100).min(2),
      email: z.string().email(),
      password: z.string().min(6),
      confirm_password: z.string().min(6),
      country: z.string(),
    })
    .refine((data) => data.password === data.confirm_password, {
      message: "Passwords don't match",
      path: ["confirm"],
    }),
});

export const userSchema = z.object({
  body: z.object({
    full_name: z.string().max(100).min(2).optional(),
    email: z.string().email().optional(),
    phone: z.string().optional().nullable(),
    dob: z
      .string()
      .refine((value) => parseDate(value) !== undefined, {
        message: "Invalid date format",
      })
      .transform(parseDate)
      .optional(),
    allow_promotional_information: z.boolean().optional(),
    picture: z.string().optional(),
    role: z.string().optional(),
    notification_settings: z.array(z.any()).optional(),
  }),
});
export const designerUserSchema = z.object({
  body: z.object({
    full_name: z.string().max(100).min(2).optional(),
    email: z.string().email().optional(),
    phone: z.string().optional().nullable(),
    dob: z
      .string()
      .refine((value) => parseDate(value) !== undefined, {
        message: "Invalid date format",
      })
      .transform(parseDate)
      .optional(),
    allow_promotional_information: z.boolean().optional(),
    picture: z.string().optional(),
    role: z.string().optional(),
    designer: z
      .object({
        background: z.string().optional(),
        contacts: z
          .array(z.object({ name: z.string(), value: z.string() }))
          .optional(),
        links: z.array(string()).optional(),
        order_count: z
          .object({
            assigned_orders: z.number(),
            in_progress_orders: z.number(),
            completed_orders: z.number(),
          })
          .optional(),
        skills: z
          .array(z.object({ skill_name: z.string(), level: z.number() }))
          .optional(),
        identity_documents: z
          .array(z.object({ document_name: z.string(), url: z.string() }))
          .optional(),
        portfolio: z
          .array(z.object({ project_name: z.string(), url: z.string().url() }))
          .optional(),
      })
      .optional(),
  }),
});

export const passwordChangeSchema = z.object({
  body: z
    .object({
      current_password: z.string().min(6),
      new_password: z.string().min(6),
      new_password_confirm: z.string().min(6),
    })
    .refine((data) => data.new_password === data.new_password_confirm, {
      message: "Passwords don't match",
      path: ["confirm"],
    }),
});

export const addressSchema = z.object({
  body: z.object({
    _id: z.string().optional(),
    first_name: z.string().max(100).min(2),
    last_name: z.string().max(100).min(2),
    country: z.string(),
    state: z.string(),
    city: z.string(),
    postcode: z.number(),
    address: z.string(),
  }),
});

export const newChat = z.object({
  body: z.object({
    chat_name: z.string(),
  }),
});

export const newMessage = z.object({
  body: z.object({
    message: z.string(),
    chat_id: z.string(),
  }),
});

export const chatMessages = z.object({
  body: z.object({
    chat_id: z.string(),
    page: z.number().optional(),
    limit: z.number().optional(),
  }),
});
