import { string, z } from "zod";

export const fileUploadSchema = z.object({
  body: z.object({
    file_name: z.string(),
    file_type: z.string(),
    folder: z.string(),
  }),
});

export const draftModelUploadSchema = z.object({
  body: z.object({
    file_name: z.string(),
    file_type: z.string(),
    order_id: z.string()
  }),
});