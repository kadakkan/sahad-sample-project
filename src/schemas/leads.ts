import { z } from "zod";

export const GINleadSchema = z.object({
  body: z.object({
    email: z.string().email(),
    full_name: z.string().max(100).min(2).optional(),
    phone_number: z.string().max(15).min(10).optional(),
    country: z.string().optional(),
    request_info: z.string().max(500).optional(),
    location: z
      .object({
        latitude: z.number(),
        longitude: z.number(),
        address: z.string(),
      })
      .optional(),
  }),
});

export const QleadSchema = z.object({
  body: z.object({
    email: z.string().email(),
    full_name: z.string().max(100).min(2).optional(),
    phone_number: z.string().max(20).min(10).optional(),
    country: z.string().optional().optional(),
    request_info: z.string().max(500).optional().optional(),
    location: z
      .object({
        latitude: z.number(),
        longitude: z.number(),
        address: z.string(),
      })
      .optional(),
  }),
});

export const LPleadSchema = z.object({
  body: z.object({
    email: z.string().email(),
    full_name: z.string().max(100).min(2).optional(),
    phone_number: z.string().max(20).min(10).optional(),
    country: z.string().optional(),
    request_info: z.string().max(500).optional(),
    location: z
      .object({
        latitude: z.number(),
        longitude: z.number(),
        address: z.string(),
      })
      .optional(),
  }),
});
